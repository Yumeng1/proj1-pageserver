"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  FIXME:
  Currently this program always serves an ascii graphic of a cat.
  Change it to serve files if they end with .html or .css, and are
  located in ./pages  (where '.' is the directory from which this
  program is run).
"""
import os
DOCROOT = "./pages"
import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##
CAT = """
     ^ ^
   =(   )=
"""

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))

    parts = request.split()
    souce_webpath = os.path.join(DOCROOT, parts[1][1:])
    if len(parts) > 1 and parts[0] == "GET":
        # first check if the name is end by html or css
        # if yes then sent the name to next check 
        # if not then transmit unhandled request
        if (".html" in parts[1]) or (".css" in parts[1]):
            # to get the last part of the inpu name
            web = parts[1]
            # use in methond to find if the name contains the 
            # invalid symbol 
            # if contains then transmit the 403 forbidden error 
            # if     not then send to the next check
            if ((web.find("`") == -1) and (web.find("..") == -1) and (web.find("//") == -1)):
                # use os.path.join methon   
                # using os.join to join the path 
                # for example os.path.join("c","foo") => c:foo
                # make the path looks like ./name.html
                # use os.path.isfile to check if the path is already in
                # the current dictionary
                path_exist_check = os.path.isfile(souce_webpath)
                # if True transmit the ok and the cat 
                # if False tranmit the 404 not found error
                if path_exist_check == True:
                    # correct_page = open(souce_webpath, 'r', encoding='utf-8')
                    # # transmit(STATUS_OK, sock)
                    # message = correct_page.read()
                    # print("\n!!message is here: ", message)
                    try: 
                        with open(souce_webpath, 'r', encoding='utf-8') as source:
                            string = ""
                            for line in source:
                                string += line
                            transmit(STATUS_OK,sock)
                            transmit(string,sock)
                    except OSError as error:
                        log.warn("Failed to open or read file")
                        log.warn("Requested file was {}".format(souce_webpath))
                        log.warn("Exception: {}".format(error))
                elif path_exist_check == False:
                    transmit(STATUS_NOT_FOUND + "404 not found",sock)
            else:
                transmit(STATUS_FORBIDDEN +"403 forbidden", sock)

    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
